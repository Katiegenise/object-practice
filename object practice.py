#This is running on my computer as a python file, I know most of this will not be python, and having everything as comments may be hard to read
#So I'm just going to type it all out and the file will produce a lot of errors probably. Sorry

#Math comprehension section
    Sets

        Is this a valid statement given the definition of the integer and rational number?
            I think you could technically say that all of the integers are a subset of the rational numbers,
            because the integer one is the same as the rational number 2/2 or 45/45, just written differently.
            So, N woud be a subset of Q because all integers can also be rational numbers, but not all rational numbers
            can be integers.

        Is there a way that the integers can be a super-set of the rationals?
            No, because not all rationals can be integers.

        What is Omega if the elements of the set are the integers?
            Omega would be every single integer in the set of all integers. So itd be positive and negative infinity.

            Set of my favorite things in no particular order:
                1. Lord Huron
                2. Croissants
                3. My dog
                4. Monstera plants
                5. Picnics

            S = {"Lord Huron", "Croissants", "My dog", "Monstera plants", "Picnics"}

            #I am slightly confused by the concept of abstraction, so I may be doing this part wrong
            Set of things that could be enjoyed outside = {"Lord Huron", "Croissants", "My dog", "Monstera plants", "Picnics"}

    Functions
        How many sets are needed to define "Identity"?
            Only one, you are saying t is equivalent to itself
        Now write the "verbose" version of this function.
            f: a c A -> a c A #I'm using c as the subset symbol here
            The identity function takes every element a and maps it to an equivalent element in itself
        What is the domain of "Identity"?
            The domain would be a
        What is the range of "Identity"?
            The range would also be a


        How many sets are needed to define "Reversal"?
            Just one? Only the set A?
        Write the "verbose" version of this function
        What is the smallest value of A?
        What is the largest value of A?
        What is the distance of the element "1" from 3 and 4?
        What does reversal map the element "1" to?
        What, in your words, does this function do?
        Can you re-write the definition of "Reversal" to be more clear?

        #Honestly, I had some trouble with these questions. I'm really confused about what the reversal function does
        


    Logic
        The powerful Boolean algebra finds its basis on a very simple set: {True, False}.
        Consider adding a third element to the set {Maybe}. Try recreating basic logical operations using this new set of three.
            1+1 = 2 is True


        Write down the truth table of the "logical and" given two operands (X, Y)
            XX False
            XY True
            YX True
            YY False
        Write down the truth table of the "logical or" given two operands (X, Y)
            XX True
            XY True
            YX True
            YY True

        Suppose you are solving a maze.
        Just like the many people who twist the idea of True and False to 1 and 0, we can similarly twist the idea of True and False to "left" and "right".
        What sequence of True and False solves this maze?
            True
            False
            False
            False
            True
            True
            True
            False
            True
            True
            False
            False
            False
            True
            True
            False
            False
            False
            True
            True
            False
            True
            False
         What assumption(s) did you make, if any, to solve this problem?
            I assumed that each time I say to turn, the mouse will walk that direction until it hits a wall then stop until it turns to go the direction I tell it,
            and that it wont turn around at all and only face the direction it is walking.




# Here is my student object that has the ability to read, the object will print the name of the book the student is reading
class Student:

	def __init__(self, name: str, age: int, favColor: str) -> None:
		self.name = name
		self.age = age
		self.favColor = favColor

    def read(self, book: str) -> None:
        print(book)
        return

Katie = student(name = "Katie", age = 21)
Katie.read("Tell the Wolves I'm Home")

